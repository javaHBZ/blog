package org.bt.bloy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BloyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BloyApplication.class, args);
    }

}
